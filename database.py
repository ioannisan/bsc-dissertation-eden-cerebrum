#!/usr/bin/python
import psycopg2
from config import config
from sqlalchemy import create_engine, bindparam
from sqlalchemy import Column, String, Numeric, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Table, Column, String, MetaData
from sqlalchemy import Column, ForeignKey, Integer, Table, text
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
import pandas as pd
import numpy as np
import datetime


Base = automap_base()

params = config()
db_string = "postgresql://{user}:{password}@{host}:5432/{database}".format(
    user=params['user'], password=params['password'], host=params['host'], database=params['database'])
db = create_engine(db_string)

#Base.prepare(autoload_with=db)

# mapped classes are now created with names by default
# matching that of the table name.
#Post = Base.classes.posts
#Topic = Base.classes.topics

session = Session(db)
base = declarative_base()

"""
post_topics = Table(
    "post_topics",
    base.metadata,
    Column("post_id", ForeignKey("public.post.id"), primary_key=True),
    Column("topic_id", ForeignKey("public.topic.id"), primary_key=True),
    schema='public'
)
"""

"""
class Post(base):
    __tablename__ = 'posts'
    __schema__ = {'schema': 'public'}

    id = Column(Integer, primary_key=True)
    body = Column(String)
    date_created = Column(DateTime)
    likes = Column(Integer)
    topics = relationship(
        "Topic", secondary=post_topics, backref="Post"
    )

class Topic(base):
    __tablename__ = 'topics'
    __schema__ = {'schema': 'public'}
    id = Column(Integer, primary_key=True)
    title = Column(String)
    date_created = Column(DateTime)
"""

#Session = sessionmaker(db)
#session = Session()


def get_post_names(ids):
    sql_query = """SELECT p.id, p.user_id, p.date_created, p.likes, p.body, t.title
                            FROM POSTS p
                            INNER JOIN POST_TOPICS pt
                                ON pt.post_id = p.id
                            INNER JOIN TOPICS t
                                ON t.id = pt.topic_id
                        WHERE p.id IN :ids"""
    params = {'ids': ids, }
    # session is a session object from sqlalchemy
    t = text(sql_query)
    t = t.bindparams(bindparam('ids', expanding=True))
    result = session.execute(t, params)
    result_as_list = result.fetchall()
    post_data = pd.DataFrame(result_as_list, columns=[
                             'id', 'user_id', 'date_created', 'likes', 'body', 'category'])
    return post_data


def getPosts():
    sql_query = text("""SELECT p.id, p.user_id, p.date_created, p.likes, p.body, t.title
                        FROM POSTS p
                        INNER JOIN POST_TOPICS pt
                            ON pt.post_id = p.id
                        INNER JOIN TOPICS t
                            ON t.id = pt.topic_id
                        INNER JOIN USER_TOPICS ut
                            on p.user_id = ut.user_id """)

    result = session.execute(sql_query)

    result_as_list = result.fetchall()
    post_data = pd.DataFrame(result_as_list, columns=[
                             'id', 'user_id', 'date_created', 'likes', 'body', 'category'])
    print(post_data)
    return post_data


def is_already_friends(user_a, user_b):
    sql_query = """
        select f
        from Friendship f
        where (f.requester_id= :requester and f.addressee_id= :addressee)
        or (f.requester_id = :addressee and f.addressee_id = :requester);
    """
    params = {'requester': user_a, 'addressee': user_b}
    t = text(sql_query)

    result = session.execute(t, params)
    result_as_list = result.fetchall()
    if len(result_as_list) > 0:  # if result is not empty, both users are already friends
        return True
    return False


def get_user_friends(user_id):
    sql_query = """
        SELECT distinct f.created_at, f.requester_id, f.addressee_id
        FROM friendship f
        WHERE f.requester_id = :user_id or f.addressee_id=:user_id;"""

    param = {'user_id': user_id, }
    t = text(sql_query)
    t = t.bindparams(bindparam('user_id', expanding=False))
    result = session.execute(t, param)
    result_as_list = result.fetchall()

    user_friends = []
    for result in result_as_list:
        epoch = int(result[0].timestamp())
        result = (result[1], result[2])  # tuple of 2 user ids
        for item in result:
            if item not in user_friends and item != user_id:
               friendship = {
                   'date': epoch,
                   'friend': item
               }
               user_friends.append(friendship)

    def sort_rule(item):
        return item['date']

    user_friends.sort(reverse=True, key=sort_rule)

    return user_friends


def getUsers():
    sql_query = text("""SELECT user_id, topic_score FROM user_topic_scores""")
    sql_user_scalar = text("""SELECT extract(epoch from u.date_of_birth) as dob, u.gender
                                FROM users u""")
    result = session.execute(sql_query)

    result_scalar = session.execute(sql_user_scalar)
    result_as_list = result.fetchall()
    result_scalar_as_list = result_scalar.fetchall()
    user_scalar = pd.DataFrame(
        result_scalar_as_list, columns=['dob', 'gender'])
    user_prefs = pd.DataFrame(result_as_list, columns=[
                              'user_id', 'topic_score'])

    user_ids = user_prefs.user_id.unique().tolist()

    vectors = []

    for user_id in user_ids:
        user_pref = user_prefs[user_prefs['user_id']
                               == user_id].topic_score.to_numpy()
        # normalize the vector for the cosine similarity to apply if needed
        #user_pref = user_pref / np.linalg.norm(user_pref)
        vectors.append(user_pref)

    user_scalar['dob'] = [int(dob) for dob in user_scalar['dob']]

    return user_ids, vectors, user_scalar.dob.tolist(), user_scalar.gender.tolist()


def get_user_vector(user_id):
    sql_query = """SELECT user_id, topic_score FROM user_topic_scores WHERE user_id= :user_id"""

    params = {'user_id': user_id, }
    t = text(sql_query)
    t = t.bindparams(bindparam('user_id', expanding=False))
    #result = session.execute(t, params)
    result = sessionExchange(t, params)
    result_as_list = result.fetchall()

    user_prefs = pd.DataFrame(result_as_list, columns=[
                              'user_id', 'topic_score'])
    user_pref = user_prefs.topic_score.to_numpy()
    user_pref = user_pref / np.linalg.norm(user_pref) #normalize vector

    #return user vector
    return user_pref


#posts = session.query(Post, Topic).select_from(Post).join(Topic, Post.id == Topic.id).all()
#for post in posts:
    #print(type(post))
#    print(post.topics.title)
    #for topic in post.topics:
    #print(topic.title)
    #print(post.body)
    #print(post.body)

def connect():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = config()

        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)

        # create a cursor
        cur = conn.cursor()

	# execute a statement
        print('PostgreSQL database version:')
        cur.execute('SELECT version()')

        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

	# close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

def sessionExchange(query, params=None):
    db = create_engine(db_string)
    session = Session(db)
    result = []
    try:
        if params:
            result = session.execute(query, params)
        else:
            result = session.execute(query)
        session.commit()
    except:
        session.rollback()
    finally:
        session.close()
        return result
#if __name__ == '__main__':
    #is_already_friends(122, 411)
    #get_user_friends(122)
