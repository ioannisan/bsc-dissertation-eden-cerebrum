# BSc Dissertation Project/Recommendations Unit - Eden Cerebrum

Complete Code Repository for the recommendations unit of Eden Architecture. The models are excluded due to git size limitations. 
Contains a Dockerfile which can be used to build the container. It requires connection with the database infrastructure.


This project uses the *HuggingFace* Library and *FAISS* Library

## Development server

All services run on the default Flask development server. Although gunicorn is installed as a dependency. Migration to gunicorn will be the next step

## Docker Build

Run `docker build .` to generate a new docker image. You can also use `docker run -p 5000:5000 -p 6000:6000 -p 4000:4000 -d
docker.io/edencerebrum/edencerebrum_docker` to generate and run the docker container for the services.



<div align="center">

<sub>Built with ❤︎ by Ioannis Anastasopoulos</sub>
</div>


## Contact
Ioannis Anastasopoulos - giannisanast34@gmail.com
