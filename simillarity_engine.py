import faiss
import numpy as np

class SimilarityEngine:

    def __init__(self, vectors):
        self.vectors = np.float32(vectors)
        d = self.vectors.shape[1]
        #self.index = faiss.IndexFlatL2(d)
        #self.index.add(self.vectors)
        #12 and 4 is also a good combination
        #16 and 4 is the best so far yet it requires more training samples
        self.index = faiss.index_factory(d, "IVF8_HNSW4,Flat")
        self.index.train(self.vectors)
        self.index.add(self.vectors)
        self.cosine = faiss.IndexFlatL2(d)
        self.cosine.add(self.vectors)

    def search_similarity(self, vector, number_of_candidates=5):
        query = np.array([vector])
        D, I = self.index.search(np.float32(query), number_of_candidates)
        Dc, Ic = self.cosine.search(np.float32(query), number_of_candidates)
        return I, Ic
