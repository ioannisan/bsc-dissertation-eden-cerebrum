FROM ubuntu:18.04
FROM continuumio/miniconda3

RUN apt-get update && \
      apt-get -y --no-install-recommends install sudo && \
      apt-get -y install libpq-dev gcc && \
      sudo apt-get -y --no-install-recommends install libsndfile1-dev && \
      sudo rm -rf /var/lib/apt/lists/*

WORKDIR /app
#WORKDIR .

# Create the environment:
COPY environment.yml .
RUN conda env create -f environment.yml

# Activate the environment, and make sure it's activated:
SHELL ["conda", "run", "-n", "topicModeler", "/bin/bash", "-c"]
# RUN conda activate topicModeler
RUN echo "Make sure flask is installed:"
RUN python -c "import flask"
RUN python -c "import transformers"
RUN python -c "import faiss"

COPY . /app

EXPOSE 4000 5000 6000

RUN ["chmod", "+x", "/app/gunicorn.sh"]
ENTRYPOINT ["/app/gunicorn.sh"]
#CMD [“python”, app/people_to_people_recommender.py”]  #application to be run
#CMD [“python”, app/post_recommender.py”]  #application to be run
#CMD [“python”, app/classifier.py”]  #application to be run
