from transformers import BertForSequenceClassification
import torch
from transformers import BertTokenizer
from flask import Flask, request, jsonify
from comment_sentiment_analysis import sentiment_analyst as sa
import warnings
import torch.nn as nn

warnings.filterwarnings("ignore")

#MAIN_PATH = 'post_classifier/V5_General_finetuned_BERT_epoch_3.model'
MAIN_PATH = 'post_classifier/V6_General_finetuned_BERT_epoch_6.model'
CULTURE_PATH = 'post_classifier/V2_Culture_finetuned_BERT_epoch_2.model'
HEALTH_PATH = 'post_classifier/V2_Health_finetuned_BERT_epoch_2.model'
SPORTS_PATH = 'post_classifier/V2_Sports_finetuned_BERT_epoch_2.model'
POLITICS_PATH = 'post_classifier/V2_Politics_finetuned_BERT_epoch_2.model'
TECHNOLOGY_PATH = 'post_classifier/V2_Technology_finetuned_BERT_epoch_2.model'


base_dict = {
    "Social and Politics": 0,
    "Sports and Athletism": 1,
    "Technology and Science": 2,
    "Culture and Enterainment": 3,
    "Health and Wellbeing": 4
}

base_unique_classes = [
    'politics',
    'sports',
    'technology',
    'culture',
    'health'
]


culture_dict = {
    'Music': 0,
    'Tv & Movies': 1,
    'Modern and classical arts': 2,
    'History': 3,
}

culture_unique_classes = [
    'Music',
    'Tv & Movies',
    'Modern and classical arts',
    'History'
]

health_dict = {
    'Healthy Life & Lifestyle': 0,
    'Medicine & Pandemics': 1,
    'Mental Health & Addictions': 2
}

health_unique_classes = [
    'Healthy Life & Lifestyle',
    'Medicine & Pandemics',
    'Mental Health & Addictions'
]

sports_dict = {
    "Football": 0,
    "General & Miscelaneous Sports": 1,
    "Tennis": 2,
    "Basketball & NBA": 3,
    "Racing Sports": 4
}

sports_unique_classes = [
    'Football',
    'General & Miscelaneous Sports',
    'Tennis',
    'Basketball & NBA',
    'Racing Sports'
]

politics_dict = {
    'International & Foreign Politics': 0,
    'Miscelaneous Politics & Legislation': 1,
    'Activism': 2,
    'Economy': 3,
    'Health & Politics': 4,
    'Environment': 5
}

politics_unique_classes = [
    'International & Foreign Politics',
    'Miscelaneous Politics & Legislation',
    'Activism',
    'Economy',
    'Health & Politics',
    'Environment'
]

technology_dict = {
    'Artificial Intelligence': 4,
    'Cryptocurrencies': 0,
    'General Technology News': 1,
    'Space & Science': 3,
    'Technology Products': 2
}

technology_unique_classes = [
    'Cryptocurrencies',
    'General Technology News',
    'Technology Products',
    'Space & Science',
    'Artificial Intelligence'
]


device = 'cpu'


class ModelInitiator:

    def __init__(self, num_of_classes):
        self.model = BertForSequenceClassification.from_pretrained("bert-base-uncased",
                                                                   num_labels=num_of_classes,
                                                                   output_attentions=False,
                                                                   output_hidden_states=False)

    def get_model(self):
        return self.model


tokenizer = BertTokenizer.from_pretrained('bert-base-uncased',
                                          do_lower_case=True)

model = ModelInitiator(len(base_dict)).get_model()
tech_model = ModelInitiator(len(technology_dict)).get_model()
politics_model = ModelInitiator(len(politics_dict)).get_model()
sports_model = ModelInitiator(len(sports_dict)).get_model()
health_model = ModelInitiator(len(health_dict)).get_model()
culture_model = ModelInitiator(len(culture_dict)).get_model()


# Model class must be defined somewhere

sentiment_analyst = sa.SentimentAnalyst()

#Base Model (for first 5 categories)
#model.load_state_dict(torch.load(MAIN_PATH, map_location=torch.device('cpu')))
checkpoint = torch.load(MAIN_PATH, map_location=torch.device('cpu'))
model.load_state_dict(checkpoint['model_state_dict'])
#model.to('cpu')
model.eval()  # need to run eval for inference to all models, otherwise the model will give unstable predictions

#Sub modells for the rest classes
#Technology Submodel
tech_model.load_state_dict(torch.load(
    TECHNOLOGY_PATH, map_location=torch.device('cpu')))
tech_model.eval()

#Sports submodel
sports_model.load_state_dict(torch.load(
    SPORTS_PATH, map_location=torch.device('cpu')))
sports_model.eval()

#Politics submodel
politics_model.load_state_dict(torch.load(
    POLITICS_PATH, map_location=torch.device('cpu')))
politics_model.eval()

#Health submodel
health_model.load_state_dict(torch.load(
    HEALTH_PATH, map_location=torch.device('cpu')))
health_model.eval()

#Culture submodel
culture_model.load_state_dict(torch.load(
    CULTURE_PATH, map_location=torch.device('cpu')))
culture_model.eval()

models = {
    "technology": (tech_model, technology_dict, technology_unique_classes),
    "politics": (politics_model, politics_dict, politics_unique_classes),
    "sports": (sports_model, sports_dict, sports_unique_classes),
    "health": (health_model, health_dict, health_unique_classes),
    "culture": (culture_model, culture_dict, culture_unique_classes)
}

#model.load_state_dict(torch.load(PATH, map_location=torch.device('cpu')))
#model.eval() # run if you only want to use it for inference

#Pipeline for super class


def predictionPipeline(text):
    encoded_review = tokenizer.encode_plus(
        text,
        max_length=256,
        add_special_tokens=True,
        return_token_type_ids=False,
        padding=True,
        return_attention_mask=True,
        return_tensors='pt',
    )

    input_ids = encoded_review['input_ids'].to(device)
    attention_mask = encoded_review['attention_mask'].to(device)

    output = model(input_ids, attention_mask)

    _, prediction = torch.max(output[0][0], 0)

    classified_super_class = base_unique_classes[prediction]

    softmax = nn.Softmax(dim=-1)

    y = softmax(output[0][0])

    print(f'Base Model Prediction: {y}')
    print(f'Review text: {text}')
    print(f'Super Class  : {classified_super_class}')

    classified_class = submodel_prediction_pipeline(
        text, classified_super_class)

    return classified_class

#Pipeline for fine - grained classes


def submodel_prediction_pipeline(text, super_class):
    #encode input to embeddings
    encoded_text = tokenizer.encode_plus(
        text,
        max_length=256,
        add_special_tokens=True,
        return_token_type_ids=False,
        return_attention_mask=True,
        padding=True,
        return_tensors='pt'
    )

    input_ids = encoded_text['input_ids'].to(device)
    attention_mask = encoded_text['attention_mask'].to(device)

    #access the submodel
    output = models[super_class][0](input_ids, attention_mask)
    _, prediction = torch.max(output[0][0], 0)

    #access the submodel's unique classes
    classified_class = models[super_class][2][prediction]

    softmax = nn.Softmax(dim=-1)

    y = softmax(output[0][0])

    print(f'Prediction: {y}')

    print(f'Review text: {text}')
    print(f'Class  : {classified_class}')

    return classified_class


app = Flask(__name__)


def preprocess(text):
    new_text = []
    for t in text.split(" "):
        t = '@user' if t.startswith('@') and len(t) > 1 else t
        t = 'http' if t.startswith('http') else t
        new_text.append(t)
    return " ".join(new_text)


@app.route('/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        post_string = request.json["post_to_classify"]
        # remove links and mentions / light preprocessing
        post_string = preprocess(post_string)
        predicted_class = predictionPipeline(post_string)
        data = {
            'topic': predicted_class
        }

    return jsonify(data)


@app.route('/entity/analyze', methods=['POST'])
def analyze():
    if request.method == 'POST':
        body_string = request.json["entity"]
        print(body_string)
        predicted_sentiments = sentiment_analyst.predict(body_string)

        data = dict()

        for sentiment in predicted_sentiments:
            data[sentiment] = predicted_sentiments[sentiment]

    return jsonify(data)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=6000, debug=True)
