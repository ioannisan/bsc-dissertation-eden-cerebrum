#!/bin/bash --login
#gunicorn --chdir app people_to_people_recommender:recommender_friends -w 2 --threads 2 -b 0.0.0.0:80
conda activate topicModeler

#gunicorn  wsgi_recommender_friends:people_to_people_recommender -w 2 --threads 1 -b --timeout 300 0.0.0.0:4000
#gunicorn  wsgi_classifier:classifier -w 2 --threads 2 -b 0.0.0.0:5000 --timeout 300
#gunicorn  wsgi_recommender:post_recommender -w 2 --threads 1 -b 0.0.0.0:6000 --timeout 300


exec python ./people_to_people_recommender.py &
exec python ./classifier.py &
exec python ./post_recommender.py
