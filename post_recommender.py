from topic_modeling import bert_topic as bt
from flask import Flask, request, jsonify
import inflect

p = inflect.engine()
#from nltk.stem import WordNetLemmatizer
#from nltk.stem.lancaster import LancasterStemmer

topic_model = bt.TopicModel()
topic_model.load()

app = Flask(__name__)


@app.route('/find/topics', methods=['POST'])
def topics():
    if request.method == 'POST':
        interests = request.json['user_interests']
        keywords = []
        for interest in interests:
            words = interest.split('&')
            for word in words:
                if len(word) > 1:
                    singular_word = p.singular_noun(word.lower())
                    if singular_word:
                        keywords.append(singular_word)
                    else:
                        keywords.append(word.lower())

        print(keywords)
        #print(interests)
        user_topics = []
        for interest in keywords:
            topics = topic_model.find_topic(interest)
            for topic in topics:
                if topic not in user_topics and topic != -1:
                    related_topics = topic_model.extract_topic_keywords(topic)
                    print(related_topics[0])
                    user_topics.append(topic)
            #user_topics.append([topic for topic in topics if topic not in user_topics])
        print(user_topics)
        data = {
            'user_topics': user_topics
        }

    return jsonify(data)


@app.route('/cluster/post', methods=['POST'])
def cluster():
    if request.method == 'POST':
        interests = request.json['post_to_cluster']

        topics, _ = topic_model.transform(interests)

        #print(topics)
        data = {
            'post_topics': int(topics[0])
        }

    return jsonify(data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
